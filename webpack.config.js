const path = require('path');
const webpack = require('webpack');

module.exports = {
	context: path.resolve(__dirname, "src"),
    entry: {
        index: './index.jsx',
        vendor: ["axios", "blueimp-md5", "js-storage", "lodash", "react", "react-autobind", "react-dom", "react-router", "react-router-dom"]
    },
    output: {
        path: path.join(__dirname, './assets/js'),
        filename: '[name].js'
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /(node_modules|bower_components|public)/,
            use: "babel-loader"
        }, {
            test: /\.json$/,
            use: 'json-loader'
        }, {
	        test: /\.svg$/,
	        use: 'svg-loader?pngScale=2'
        }, {
			test: /\.css$/,
			use: ['style-loader', 'css-loader']
            }
        ]
    },
    resolve: {
        modules: [path.resolve(__dirname, "src"), 'node_modules'],
        extensions: ['.jsx', '.json', '.css', '.js'],
	    alias: {
		    Utils: path.resolve(__dirname, 'src/utils/'),
		    Theme: path.resolve(__dirname, 'src/theme/'),
		    Pages: path.resolve(__dirname, 'src/pages/'),
		    Config: path.resolve(__dirname, 'src/config/'),
		    Comps: path.resolve(__dirname, 'src/components/'),
		    Apps: path.resolve(__dirname, 'src/apps/'),
		    Svg: path.resolve(__dirname, 'assets/img/svg/'),
	    }
    },
	plugins: [
		//new webpack.optimize.UglifyJsPlugin(),
		new webpack.optimize.CommonsChunkPlugin({
			name: ['common', 'vendor'],
			minChunks: 2
		}),
		new webpack.DefinePlugin({
            //define constants here, will be replaced in all text to its value
		}),
		new webpack.ProvidePlugin({
			//set libraries autoload
            md5: 'blueimp-md5',
            storage: 'js-storage',
            _: 'lodash'
		})
	],
	devtool: 'source-map'
};