import React from 'react';
import autoBind from 'react-autobind';
import { deepOrange } from 'material-ui/colors';

class DisplayError extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			message: this.props.message ? this.props.message : 'Произошла ошибка...',
			color: this.props.color ? this.props.color : deepOrange['A400']
		};
		autoBind(this);
	}

	render() {
		return <div className="inline-alert" style={{borderColor: this.state.color, color: this.state.color}}>{this.state.message}</div>
	}
}

export default DisplayError;