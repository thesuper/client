import React from 'react';
import ReactDOM from 'react-dom';
import ClientApp from './apps/client';

ReactDOM.render(<ClientApp />, document.querySelector('#clientapp-root'));