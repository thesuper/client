import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import autoBind from 'react-autobind';
import IndexPage from 'Pages/client/index';
import DisplayError from 'Utils/common_error';
import theme from "Theme/client/overrides";
import { MuiThemeProvider } from 'material-ui/styles';

class ClientApp extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			error: false
		};
		autoBind(this);
	}

	componentDidCatch() {
		this.setState({
			error: true
		});
	}

	render() {
		if (this.state.error) {
			return <DisplayError />;
		} else {
			return (
				<div className="client-app-container">
					<MuiThemeProvider theme={theme}>
						<IndexPage />
					</MuiThemeProvider>
				</div>
			);
		}
	}
}

export default ClientApp;