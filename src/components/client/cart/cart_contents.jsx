import React from "react";
import autoBind from "react-autobind";
import {withStyles} from "material-ui/styles/index";
import Drawer from 'material-ui/Drawer';
import List, { ListItem, ListItemAvatar, ListItemText, ListItemSecondaryAction } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import ShadowScrollbars from 'Comps/client/ui/shadow_scrollbars';
import Typography from 'material-ui/Typography';
import RoubleIcon from 'Comps/client/cart/rur';
import QtyInput from 'Comps/client/ui/qty';
import SubTotal from 'Comps/client/cart/subtotal_small';

const drawerWidth = 320;

const styles = theme => ({
	drawerPaper: {
		width: drawerWidth,
		height: '100%',
		boxSizing: 'border-box',
		zIndex: theme.zIndex.appBar-10,
		paddingTop: 64
	},
	listIcon: {
		color: theme.palette.primary[500],
		"&:hover": {
			color: theme.palette.getContrastText(theme.palette.primary[500]),
			backgroundColor: theme.palette.primary[700]
		}
	},
	buttonOrder: {
		margin: `${theme.spacing.unit}px ${theme.spacing.unit*2}px`
	},
	buttonClose: {
		margin: `${theme.spacing.unit}px ${theme.spacing.unit*2}px`
	},
	titleNum: {
		color: theme.palette.iconsPrimary,
		float: 'right'
	},
/*	rootTitle: {
		padding: '24px 24px 0 24px',
		fontSize: 24,
		fontWeight: 'normal',
		lineHeight: '24px',
		margin: 0
	},*/
	listItem: {
		padding: '8px 16px 8px 16px'
	},
	listItemRoot: {
		width: 'calc(100% - 64px)',
		padding: '0 8px 0 0',
		boxSizing: 'border-box'
	},
	listItemTitle: {
		height: 18,
		lineHeight: '18px',
		fontSize: 18,
		fontWeight: 500,
		marginBottom: 8,
		padding: 0
	},
	listItemText: {
		color: theme.palette.commonLite,
		fontSize: 12,
		height: 12,
		lineHeight: '14px',
		padding: 0,
	},
	listAvatar: {
		borderRadius: 0,
		width: 48,
		height: 48,
		'& img': {
			width: '100%',
			height: 'auto'
		}
	},
	listItemSecondaryAction: {
		display: 'block',
		clear: 'both',
		width: '100%',
		minHeight: 56,
		margin: '0 0 8px 0',
		padding: '0 16px 0 16px',
		position: 'relative',
		right: 'auto',
		top: 'auto',
		boxSizing: 'border-box',
		overflow: 'hidden'
	},
	blockHalfLeft: {
		width: '65%',
		float: 'left',
		minHeight: 44
	},
	blockHalfRight: {
		width: '35%',
		float: 'right',
		minHeight: 44,
		textAlign: 'right',
		position: 'relative'
	},
	priceLabel: {
		color: theme.palette.commonLite,
		fontSize: 12,
		height: 12,
		lineHeight: '14px',
		marginBottom: 4
	},
	priceValue: {
		boxSizing: 'border-box',
		padding: '6px 18px 0 0',
		fontWeight: 'bold',
		fontSize: 18,
		lineHeight: '18px',
		textAlign: 'right'
	},
	priceCurrency: {
		position: 'absolute',
		right: 0,
		top: 24,
		width: 15,
		height: 15,
		'.cart-subtotal &': {
			top: -2
		}
	},
	itemsList: {
		padding: 0
	},
	scrollable: {
		top: theme.dimensions.appBarHeight,
		left: 0,
		right: 0,
		bottom: 101,
		position: 'absolute',
		'@media only screen and (max-height: 329px)': {
			bottom: 0
		}
	}
});

class CartContents extends React.Component {

	constructor(props) {
		super(props);

		this.scrollbar = null;

		autoBind(this);
	}

	componentDidUpdate(prevProps, prevState) {
		clearTimeout(this.scrollbar);
		this.scrollbar = setTimeout(() => {
			if (this.refs.CartScrollbars) {
				this.refs.CartScrollbars.forceUpdate();
			}
		}, 1000);


	}

	render() {
		const { classes } = this.props;

		return (
			<Drawer anchor="right"
			        open={this.props.open}
			        onClose={ this.props.onClose }
			        classes={{ paper: classes.drawerPaper }}
			        type="persistent">
				<div className={classes.scrollable}>
				<ShadowScrollbars className={'cart-scrollable'} style={{height:'100%'}} ref="CartScrollbars">
					{/*<Typography type="title" classes={{title: classes.rootTitle}} gutterBottom>
						Товаров в корзине <span className={classes.titleNum}>3</span>
					</Typography>*/}
					<List className={classes.itemsList}>
						<ListItem className={classes.listItem}>
							<ListItemText
								disableTypography
								className={ classes.listItemRoot}
								primary={<Typography noWrap className={ classes.listItemTitle } type="title" gutterBottom>EQUIPOISE от SP Laboratories</Typography>}
								secondary={<Typography noWrap className={ classes.listItemText } gutterBottom>10 ml × 50 mg/ml от SP Laboratories</Typography>}
							/>
							<ListItemAvatar>
								<Avatar className={classes.listAvatar}>
									<img alt="" src={'/assets/img/categories/masteron.png'}/>
								</Avatar>
							</ListItemAvatar>
							<ListItemSecondaryAction className={classes.listItemSecondaryAction}>
								<div className={classes.blockHalfLeft}>
									<QtyInput min={1} max={10} />
								</div>
								<div className={classes.blockHalfRight}>
									<Typography noWrap className={ classes.priceLabel }>Цена</Typography>
									<Typography noWrap className={ classes.priceValue }>950</Typography>
									<RoubleIcon className={ classes.priceCurrency } />
								</div>
							</ListItemSecondaryAction>
						</ListItem>
						<Divider component="li" />

						<ListItem className={classes.listItem}>
							<ListItemText
								disableTypography
								className={ classes.listItemRoot}
								primary={<Typography noWrap className={ classes.listItemTitle } type="title" gutterBottom>NANDROGED-PH от EPF</Typography>}
								secondary={<Typography noWrap className={ classes.listItemText } gutterBottom>10 ml × 100 mg/ml</Typography>}
							/>
							<ListItemAvatar>
								<Avatar className={classes.listAvatar}>
									<img alt="" src={'/assets/img/categories/nandrolon.jpg'}/>
								</Avatar>
							</ListItemAvatar>
							<ListItemSecondaryAction className={classes.listItemSecondaryAction}>
								<div className={classes.blockHalfLeft}>
									<QtyInput min={1} value={5} max={10} />
								</div>
								<div className={classes.blockHalfRight}>
									<Typography noWrap className={ classes.priceLabel }>Цена</Typography>
									<Typography noWrap className={ classes.priceValue }>2399</Typography>
									<RoubleIcon className={ classes.priceCurrency } />
								</div>
							</ListItemSecondaryAction>
						</ListItem>
						<Divider component="li" />

						<ListItem className={classes.listItem}>
							<ListItemText
								disableTypography
								className={ classes.listItemRoot}
								primary={<Typography noWrap className={ classes.listItemTitle } type="title" gutterBottom>SP Propinate</Typography>}
								secondary={<Typography noWrap className={ classes.listItemText } gutterBottom>10 ml × 100 mg/ml</Typography>}
							/>
							<ListItemAvatar>
								<Avatar className={classes.listAvatar}>
									<img alt="" src={'/assets/img/categories/oksimetalon.png'}/>
								</Avatar>
							</ListItemAvatar>
							<ListItemSecondaryAction className={classes.listItemSecondaryAction}>
								<div className={classes.blockHalfLeft}>
									<QtyInput min={1} value={2} max={10} />
								</div>
								<div className={classes.blockHalfRight}>
									<Typography noWrap className={ classes.priceLabel }>Цена</Typography>
									<Typography noWrap className={ classes.priceValue }>1985</Typography>
									<RoubleIcon className={ classes.priceCurrency } />
								</div>
							</ListItemSecondaryAction>
						</ListItem>
						<Divider component="li" />

						<ListItem className={classes.listItem}>
							<ListItemText
								disableTypography
								className={ classes.listItemRoot}
								primary={<Typography noWrap className={ classes.listItemTitle } type="title" gutterBottom>SP Propinate</Typography>}
								secondary={<Typography noWrap className={ classes.listItemText } gutterBottom>10 ml × 100 mg/ml</Typography>}
							/>
							<ListItemAvatar>
								<Avatar className={classes.listAvatar}>
									<img alt="" src={'/assets/img/categories/oksimetalon.png'}/>
								</Avatar>
							</ListItemAvatar>
							<ListItemSecondaryAction className={classes.listItemSecondaryAction}>
								<div className={classes.blockHalfLeft}>
									<QtyInput min={1} value={2} max={10} />
								</div>
								<div className={classes.blockHalfRight}>
									<Typography noWrap className={ classes.priceLabel }>Цена</Typography>
									<Typography noWrap className={ classes.priceValue }>1985</Typography>
									<RoubleIcon className={ classes.priceCurrency } />
								</div>
							</ListItemSecondaryAction>
						</ListItem>

					</List>
					<SubTotal className={'subtotal-inline'} />
				</ShadowScrollbars>
				</div>
				<SubTotal className={'subtotal-fixed'} />
			</Drawer>
		);
	}
}

export default withStyles(styles, { withTheme: true })(CartContents);