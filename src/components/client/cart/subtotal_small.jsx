import React from 'react';
import {withStyles} from "material-ui/styles/index";
import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';
import DoneIcon from 'material-ui-icons/Done';
import RoubleIcon from 'Comps/client/cart/rur';
import Button from 'material-ui/Button';

const styles = theme => ({
	value: {
		boxSizing: 'border-box',
		padding: '0 18px 0 0',
		fontWeight: 'bold',
		fontSize: 18,
		lineHeight: '14px',
		textAlign: 'right',
		position: 'relative',
		top: -2
	},
	currency: {
		position: 'absolute',
		right: 0,
		top: -2,
		width: 15,
		height: 15
	},
	root: {
		width: '100%'
	},
	row: {
		margin: 16,
		position: 'relative',
	},
	total: {
		display: 'block',
		float: 'left',
		boxSizing: 'border-box',
		padding: 0,
		fontSize: 14,
		lineHeight: '14px',
		marginBottom: 16
	},
	button: {
		margin: 0,
		boxShadow: theme.palette.raisedPrimaryShadow,
		borderRadius: theme.other.raisedPrimaryRadius,
		height: 32,
		lineHeight: '18px',
		width: '100%',
		boxSizing: 'border-box',
		position: 'relative',
		fontSize: 18,
		padding: '8px 16px 8px 52px'
	},
	btnIcon: {
		position: 'absolute',
		left: 12,
		width: 36,
		height: 36
	},
	divider: {
		height: 3
	}
});

class SubTotal extends React.Component {

	render() {
		const { classes } = this.props;
		const className = this.props.className ? ` ${this.props.className}` : '';

		return <div className={`${classes.root}${className}`}>
			<Divider component="li" className={ classes.divider } />
			<div className={classes.row}>
				<Typography className={classes.total}>Итого к оплате:</Typography>
				<div className={classes.value}>37895</div>
				<RoubleIcon className={ classes.currency } />
				<Button className={classes.button} raised color="primary">
					<DoneIcon className={classes.btnIcon} />
					Оформить заказ
				</Button>
			</div>
		</div>

	}
}

export default withStyles(styles, { withTheme: true })(SubTotal);