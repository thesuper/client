import React from 'react';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import ShoppingCart from 'material-ui-icons/ShoppingCart';
import autoBind from "react-autobind";
import Badge from 'material-ui/Badge';

const drawerWidth = 280;

const styles = theme => ({
	cartButton: {
		margin: '0 0 0 20px',
		[theme.breakpoints.down('md')]: {
			margin: 0,
			maxWidth: 48,
			minWidth: 48
		},
		float: 'right',
		height: 36,
		lineHeight: '20px'
	},
	cartTitle: {
		[theme.breakpoints.down('md')]: {
			display: 'none'
		},
		marginRight: theme.spacing.unit / 2
	},
	cartIcon: {
		display: 'none',
		[theme.breakpoints.down('md')]: {
			display: 'inline-block'
		},
		height: 20,
		width: 20
	},
	cartBadge: {
		backgroundColor: theme.palette.iconsPrimary
	},
	raisedPrimary: {
		boxShadow: theme.palette.raisedPrimaryShadow,
		borderRadius: theme.other.raisedPrimaryRadius,
		'&:active': {
			boxShadow: theme.palette.raisedPrimaryShadowActive
		}
	}
});

class SmallCart extends React.Component {

	constructor(props) {
		super(props);
		autoBind(this);
	}

	onCartClick() {
		if (this.props.onClick) this.props.onClick();
	}

	render() {
		const { classes, theme} = this.props;
		return (
			<div>
				<Button
					raised
					color="primary"
					className={classes.cartButton}
					onClick={ this.onCartClick }
					classes={{raisedPrimary: classes.raisedPrimary}}
				>
					<Badge classes={{colorAccent: classes.cartBadge}} color="accent" badgeContent={3}>
						<span className={classes.cartTitle}>Корзина</span>
						<ShoppingCart className={classes.cartIcon} />
					</Badge>
				</Button>
			</div>
		);
	}
}

export default withStyles(styles, {withTheme: true})(SmallCart);