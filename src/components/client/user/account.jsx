import React from 'react';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import AccountCircle from 'material-ui-icons/AccountCircle';
import AddCircle from 'material-ui-icons/AddCircleOutline';
import Menu, { MenuItem } from 'material-ui/Menu';
import autoBind from "react-autobind";
import Avatar from 'material-ui/Avatar';
import List, { ListItem, ListItemSecondaryAction, ListItemText, ListItemIcon } from 'material-ui/List';
import ArrowDropDown from 'material-ui-icons/ArrowDropDown';
import Hidden from 'material-ui/Hidden';

const styles = theme => ({
	icon: {
		color: theme.palette.primary[200],
		opacity: 0.7,
		'&:hover': {
			opacity: 1
		}
	},
	userImg: {
		boxSizing: 'border-box',
		height: 36,
		width: 36,
		[theme.breakpoints.down('md')]: {
			marginRight: 24
		}
	},
	listIcon: {
		color: theme.palette.iconsPrimary
	},
	listText: {
		padding: 0
	},
	listItemRoot: {
		position: 'releative',
		paddingRight: 25
	},
	iconDropdown: {
		color: '#000',
		position: 'absolute',
		right: 7,
		top: 15,
	},
	listItemTextRoot: {
		padding: '0 9px',
		fontSize: 16
	},
	listItemTextText: {
		color: theme.palette.commonLite
	}
});

class AccountIcon extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			auth: true,
			anchorEl: null,
		};
		autoBind(this);
	}

	handleMenu(event) {
		this.setState({ anchorEl: event.currentTarget });
	};

	handleClose() {
		this.setState({ anchorEl: null });
	};

	render() {
		const { classes } = this.props,
			{ anchorEl } = this.state,
			open = Boolean(anchorEl),
			imgSrc = '/assets/img/user.jpg';

		return (
			<div>
				<Hidden mdDown>
				<List>
					<ListItem dense button onClick={this.handleMenu} className={classes.listItemRoot}>
						<Avatar className={classes.userImg} alt="" src={imgSrc} />
							<ListItemText classes={{ root: classes.listItemTextRoot, text: classes.listItemTextText }} primary={`Иван Федорович Крузенштерн`} />
							<ArrowDropDown className={classes.iconDropdown} />
					</ListItem>
				</List>
				</Hidden>
				<Hidden mdUp>
					<IconButton className={classes.icon}
					            onClick={this.handleMenu}>
						<Avatar className={classes.userImg} alt="" src={imgSrc} />
					</IconButton>
				</Hidden>
				<Menu id="account-appbar"
					anchorEl={anchorEl}
					anchorOrigin={{
						vertical: 'top',
						horizontal: 'right',
					}}
					transformOrigin={{
						vertical: 'top',
						horizontal: 'right',
					}}
					open={open}
					onClose={this.handleClose}
					elevation={2}
				>
					<MenuItem onClick={this.handleClose}>
						<ListItemIcon>
							<AccountCircle className={classes.listIcon} />
						</ListItemIcon>
						<ListItemText className={classes.listText} inset primary="Вход" />
					</MenuItem>
					<MenuItem onClick={this.handleClose}>
						<ListItemIcon>
							<AddCircle className={classes.listIcon} />
						</ListItemIcon>
						<ListItemText className={classes.listText} inset primary="Регистрация" />
					</MenuItem>
				</Menu>
			</div>
		);
	}
}

export default withStyles(styles, { withTheme: true })(AccountIcon);