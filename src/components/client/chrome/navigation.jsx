import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Logo from 'Comps/client/chrome/logo';
import Drawer from 'material-ui/Drawer';
import Hidden from 'material-ui/Hidden';
import autoBind from "react-autobind";
import MainNavMenu from 'Comps/client/chrome/mainmenu';
import AccountIcon from 'Comps/client/user/account';
import SmallCart from 'Comps/client/cart/small_cart';
import CartContents from 'Comps/client/cart/cart_contents';
import LiveSearch from 'Comps/client/ui/live_search';
import MenuIcon from 'material-ui-icons/Menu';
import SearchIcon from 'material-ui-icons/Search';
import CloseIcon from 'material-ui-icons/Close';
import Backdrop from 'material-ui/Modal/Backdrop';

const drawerWidth = 300;

const styles = theme => ({
	appBar: {
		height: theme.dimensions.appBarHeight,
		borderBottom: '1px solid rgba(0, 0, 0, 0.1)',
	},
	tollbar: {
		padding: '0 16px',
		minHeight: theme.dimensions.appBarHeight,
	},
	navToggleIcon: {
		width: 40,
		height: 40,
		marginRight: 16,
		color: '#000'
	},
	responsiveTitles: {
		[theme.breakpoints.down('md')]: {
			display: 'none',
		},
	},
	drawerPaper: {
		width: drawerWidth,
		zIndex: theme.zIndex.appBar-10,
		padding: `64px 0 0 0`,
		boxSizing: 'border-box',
	},
	searchDrawerPaper: {
		height: theme.dimensions.appBarHeight,
		marginTop: theme.dimensions.appBarHeight,
		zIndex: theme.zIndex.appBar-30,
		boxSizing: 'border-box'
	},
	logo: {
		flex: 1
	},
	searchIcon: {
		boxSizing: 'border-box',
		width: 36,
		height: 36,
		padding: 4
	},
	searchButton: {
		marginRight: 24
	},
	backdrop: {
		backgroundColor: theme.palette.background.default,
		opacity: 0.3,
		cursor: 'no-drop'
	}
});

class ResponsiveNav extends React.Component {

	constructor(props) {
		super(props);

		const { theme } = this.props;
		this.breakpoint = theme.breakpoints.values.md;

		this.state = {
			menuOpen: this.windowWidth() >= theme.breakpoints.values.lg,
			cartOpen: false,
			searchOpen: false
		};

		autoBind(this);
	}

	windowWidth() {
		return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	}

	setStateDevice(stateUp, stateDown, callback = null) {
		let state = stateDown;
		if (this.windowWidth() >= this.breakpoint) {
			state = stateUp;
		}
		this.setState(state, callback);
	}

	onMenuClick() {
		//this.setState({ menuOpen: !this.state.menuOpen, searchOpen: false });
		const up = { menuOpen: !this.state.menuOpen, searchOpen: false };
		this.setStateDevice(up,{
			...up,
			cartOpen: false
		});
	};

	onCartClick() {
		//this.setState({ cartOpen: !this.state.cartOpen, searchOpen: false });
		const up = { cartOpen: !this.state.cartOpen, searchOpen: false };
		this.setStateDevice(up,{
			...up,
			menuOpen: false
		});
	}

	closeCart() {
		this.setState({ cartOpen: false });
	}

	onSearchClick() {
		this.setState({ searchOpen: !this.state.searchOpen, menuOpen: false, cartOpen: false });
	}

	componentDidUpdate(prevProps, prevState) {
		const scrollElement = document.querySelector('.page-client').classList,
			className = 'overflow-hidden';
		if (this.windowWidth() < this.breakpoint && (this.state.cartOpen || this.state.menuOpen) ) {
			scrollElement.add(className);
		} else {
			scrollElement.remove(className);
		}
	}

	render() {
		const { classes, theme } = this.props;
		const backdropStyle = {
			zIndex: ( this.state.menuOpen || this.state.cartOpen ? theme.zIndex.appBar-15 : -1)
		};

		return [
			<AppBar key={'navbar'} className={classes.appBar} color="inherit" elevation = {0}>
				<Toolbar className={classes.tollbar}>
					<IconButton
						aria-label="Открыть меню"
						onClick={this.onMenuClick}
						className={classes.navToggleIcon}>
						{this.state.menuOpen ? <CloseIcon /> : <MenuIcon />}
					</IconButton>
					<Typography type="title" color="inherit" className={classes.logo} noWrap>
						<Logo />
					</Typography>
					<Hidden mdUp>
						<IconButton onClick={this.onSearchClick} className={classes.searchButton}>
							{this.state.searchOpen ? <CloseIcon className={classes.searchIcon} /> : <SearchIcon className={classes.searchIcon} />}
						</IconButton>
					</Hidden>
					<AccountIcon />
					<SmallCart onClick={this.onCartClick}/>
					<Hidden mdDown>
						<LiveSearch />
					</Hidden>
				</Toolbar>
			</AppBar>,
			<Hidden key={'search-mobile'} mdUp>
				<Drawer anchor={'top'} open={this.state.searchOpen} onClose={ this.onSearchClick } type="persistent" classes={{ paper: classes.searchDrawerPaper }} SlideProps={{timeout:300,direction:'up'}}>
					<LiveSearch mobile />
				</Drawer>
			</Hidden>,
			<Drawer key={'drawer-left'}
				type="persistent"
				anchor={'left'}
				open={this.state.menuOpen}
				classes={{
					paper: classes.drawerPaper,
				}}
				onClose={this.onMenuClick}
				ModalProps={{
					keepMounted: true
				}}
			>
				<MainNavMenu />
			</Drawer>,
			<CartContents key={'cart'} open={this.state.cartOpen} onClose={this.closeCart} />,
			<Hidden key={'backdrop'} mdUp>
				<Backdrop className={classes.backdrop} onClick={ ()=>{ this.setState({ cartOpen: false, menuOpen: false }) }} invisible={true} style={backdropStyle} />
			</Hidden>,
		];
	}
}

ResponsiveNav.propTypes = {
	classes: PropTypes.object.isRequired,
	theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(ResponsiveNav);