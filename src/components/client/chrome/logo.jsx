import React from 'react';
import { withTheme } from 'material-ui/styles';
import {UI} from 'Config/client/const';

class Logo extends React.Component {

	render() {
		const { theme  } = this.props;
		return (
			<div className={'appbar-logo'}>
				{UI.applicationName}
			</div>
		);
	}
}
export default withTheme()(Logo);