import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import ShadowScrollbars from 'Comps/client/ui/shadow_scrollbars';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Collapse from 'material-ui/transitions/Collapse';
import autoBind from "react-autobind";
import ListIcon from 'material-ui-icons/List';
import CommentIcon from 'material-ui-icons/Comment';
import PayIcon from 'material-ui-icons/AccountBalanceWallet';
import ArrowDropDownIcon from 'material-ui-icons/ArrowDropDown';
import SaleIcon from 'mdi-material-ui/Sale';
import BullhornIcon from 'mdi-material-ui/Bullhorn';
import QuestionIcon from 'mdi-material-ui/CommentQuestionOutline';

const styles = theme => ({
	listRoot: {
		padding: 0
	},
	buttonText: {
		padding: 0,
		color: theme.palette.common.black,
		lineHeight: '24px',
		fontSize: 16
	},
	buttonIcon: {
		color: theme.palette.iconsPrimary,
		padding: '0 8px'
	},
	numericIcon: {
		color: theme.palette.commonLite,
		width: 40,
		paddingTop: 16,
		paddingBottom: 10,
		boxSizing: 'border-box',
		height: 40,
		marginRight: 16,
		textAlign: 'right',
		font: 'normal 14px/14px Roboto',
		'&.menu-selected': {
			color: theme.palette.common.white
		}
	},
	buttonIconClosed: {
		transform: 'rotate(-90deg)'
	},
	listSubItemText: {
		padding: 0,
		fontWeight: 500,
		fontSize: 20,
		fontFamily: 'Roboto',
		height: 20,
		lineHeight: '20px',
		whiteSpace: 'nowrap',
		width: 220,
		textOverflow: 'ellipsis',
		overflow: 'hidden'
	},
	listSubItem: {
		paddingTop: 10,
		paddingBottom: 10,
		boxSizing: 'border-box',
		height: 40
	},
	itemSelected: {
		background: theme.palette.selectedBackgroundColor,
		color: `${theme.palette.common.white} !important`,
		cursor: 'default'
	}
});

class MainMenuList extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			productsOpen: true,
			brandsOpen: false,
			curProd: 0,
			curBrand: 0
		};

		this.scrollbar = null;

		//temp rand pool
		this.pool = [];
		while (this.pool.length < 64) {
			this.pool.push(this.getRandom(0,20));
		}

		autoBind(this);
	}

	getRandom(min, max) {
		return Math.round(Math.random() * (max - min) + min);
	}

	toggleProducts() {
		this.setState({ productsOpen: !this.state.productsOpen, brandsOpen: false });
	}

	toggleBrands() {
		this.setState({ brandsOpen: !this.state.brandsOpen, productsOpen: false });
	}

	componentDidUpdate(prevProps, prevState) {
		clearTimeout(this.scrollbar);
		this.scrollbar = setTimeout(() => {
			if (this.refs.MenuScrollbar) {
				this.refs.MenuScrollbar.forceUpdate();
			}
		}, 1000);
	}

	render() {
		const {classes} = this.props;

		const categories = [
			['Болденон', 'boldenon.png'],
			['Мастерон', 'masteron.png'],
			['Метандиенон', 'metandienon.jpg'],
			['Нандролон', 'nandrolon.jpg'],
			['Оксандролон', 'oksandrolon.png'],
			['Оксиметолон', 'oksimetalon.png'],
			['Примоболан', 'boldenon.png'],
			['Станозолол', 'masteron.png'],
			['Тестостерон', 'metandienon.jpg'],
			['Тренболон', 'nandrolon.jpg'],
			['Туринабол', 'oksandrolon.png'],
			['Анастрозол', 'oksimetalon.png'],
			['Провирон', 'boldenon.png'],
			['Кленбутерол', 'masteron.png'],
			['ПКТ', 'masteron.png'],
			['Прочее', 'oksandrolon.png']
		];

		const brands = ['PARKER', 'SP Laboratories', 'Balkan Pharmaceuticals', 'Euro Prime Farmaceuticals', 'Футболки от PARKER'];

		const menu = [
			[<ListIcon/>, 'Прайс лист'],
			[<SaleIcon />, 'Скидки'],
			[<CommentIcon/>, 'Отызы'],
			[<BullhornIcon />, 'На форум'],
			[<QuestionIcon />, 'Частые вопросы'],
			[<PayIcon />, 'Как оплатить?']
		];

		return (
			<ShadowScrollbars style={{ height: '100%' }}  ref="MenuScrollbar">
				<List className={`${classes.listRoot} collapse-alt`}>
					<ListItem button onClick={this.toggleProducts}>
						<ListItemIcon className={classes.buttonIcon}>
							<ArrowDropDownIcon className={ this.state.productsOpen ? null: classes.buttonIconClosed } />
						</ListItemIcon>
						<ListItemText className={classes.buttonText} primary="Товары" />
					</ListItem>
					<Collapse component="li" in={this.state.productsOpen} timeout="auto" unmountOnExit>
						<List disablePadding subheader={<div/>}>
							{categories.map((categoryItem, index) => {
								const selected = index === this.state.curProd;
								return <ListItem
											key={index}
											className={ classes.listSubItem + (selected ? ` ${classes.itemSelected}` : '') }
											button
											component="a"
											href="#"
											onClick={ (e)=>{ e.preventDefault(); this.setState({curProd: index}); } }
										>
									<span className={selected ? `${classes.numericIcon} menu-selected` : classes.numericIcon}>{this.pool[index]}</span>
									<ListItemText disableTypography className={classes.listSubItemText}
									              primary={categoryItem[0]}/>
								</ListItem>
								}
							)}
						</List>
					</Collapse>

					<ListItem button onClick={this.toggleBrands}>
						<ListItemIcon className={classes.buttonIcon}>
							<ArrowDropDownIcon className={ this.state.brandsOpen ? null: classes.buttonIconClosed } />
						</ListItemIcon>
						<ListItemText className={classes.buttonText} primary="Бренды" />
					</ListItem>
					<Collapse component="li" in={this.state.brandsOpen} timeout="auto" unmountOnExit>
						<List disablePadding subheader={<div/>}>
							{brands.map((brandItem, index) => {
									const selected = index === this.state.curBrand;
									return <ListItem
										key={index}
										className={ classes.listSubItem + (selected ? ` ${classes.itemSelected}` : '') }
										button
										component="a"
										href="#"
										onClick={ (e)=>{ e.preventDefault(); this.setState({curBrand: index}) } }
									>
										<span className={selected ? `${classes.numericIcon} menu-selected` : classes.numericIcon}>{this.pool[index]}</span>
										<ListItemText disableTypography className={classes.listSubItemText}
										              primary={brandItem}/>
									</ListItem>
								}
							)}
						</List>
					</Collapse>

					{menu.map((menuItem, index) =>
						<ListItem key={index} button>
							<ListItemIcon className={classes.buttonIcon}>
								{menuItem[0]}
							</ListItemIcon>
							<ListItemText className={classes.buttonText} primary={menuItem[1]}/>
						</ListItem>
					)}
				</List>
			</ShadowScrollbars>
		);
	}
}

MainMenuList.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MainMenuList);