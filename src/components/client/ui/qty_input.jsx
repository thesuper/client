import React from "react";
import autoBind from "react-autobind";
import {withStyles} from "material-ui/styles/index";
import Grid from 'material-ui/Grid';
import IconButton from 'material-ui/IconButton';
import PlusIcon from 'material-ui-icons/AddCircleOutline';
import MinusIcon from 'material-ui-icons/RemoveCircleOutline';
import Input from 'material-ui/Input';
import Typography from 'material-ui/Typography';

const styles = theme => ({
	root: {
		flexGrow: 1,
	},
	item: {

	},
	icon: {
		marginTop: -theme.spacing.unit,
		opacity: 0.5,
		"&:hover": {
			opacity: 1,
			backgroundColor: theme.palette.primary[50]
		}
	},
	iconPlus: {
		marginLeft: -theme.spacing.unit
	},
	iconMinus: {

	},
	input: {
		margin: 0,
		position: 'relative',
		top: -theme.spacing.unit * 0.5,
		textAlign: 'center'
	},
	title: {
		textAlign: 'center',
		position: 'relative',
		top: theme.spacing.unit * 0.5,
	}
});

class QtyInput extends React.Component {

	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {

		const { classes } = this.props;

		return (
			<Grid container className={classes.root} justify="center">
				<Grid item xs={5} className={classes.item}>
					<Typography type="body2" className={classes.title} gutterBottom noWrap>
						{this.props.title}
					</Typography>
				</Grid>
				<Grid item xs={2} className={classes.item}>
					<IconButton className={`${classes.iconMinus} ${classes.icon}`}>
						<MinusIcon />
					</IconButton>
				</Grid>
				<Grid item xs={3} className={classes.item}>
					<Input fullWidth={true} className={classes.input} defaultValue={this.props.value} inputProps={{ style:{textAlign:'center'} }}/>
				</Grid>
				<Grid item xs={2} className={classes.item}>
					<IconButton className={`${classes.iconPlus} ${classes.icon}`}>
						<PlusIcon />
					</IconButton>
				</Grid>
			</Grid>
		);
	}
}

export default withStyles(styles, { withTheme: true })(QtyInput);