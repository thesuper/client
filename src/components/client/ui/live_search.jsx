import React from 'react';
import autoBind from 'react-autobind';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import SearchIcon from 'material-ui-icons/Search';
import IconButton from 'material-ui/IconButton';

const styles = theme => ({
	box: {
		position: 'absolute',
		width: 333,
		height: 36,
		left: 'calc(50% - 333px/2 - 132px)',
		top: 14,
		'&.mobile-ls': {
			width: '92%',
			margin: '0 4%',
			left: 0
		}
	},
	textFieldRoot: {
		padding: 0,
		'label + &': {
			marginTop: theme.spacing.unit * 3,
		}
	},
	textFieldInput: {
		width: 333,
		height: 36,
		fontSize: 16,
		lineHeight: 24,
		padding: '4px 0 4px 36px',
		boxSizing: 'border-box',
		backgroundColor: 'transparent',
		border: '1px solid rgba(0, 0, 0, 0.32)',
		borderRadius: 3,
		transition: theme.transitions.create(['border-color']),
		'&:focus': {
			borderColor: 'rgba(0, 0, 0, 0.6)',
		},
		'&::placeholder': {
			color: '#000',
			opacity: 0.7
		},
		'.mobile-ls &': {
			width: '100%'
		}
	},
	textFieldInputAlt: {
		fontSize: 18,
		paddingRight: 44,
		paddingTop: 8
	},
	icon: {
		height: 36,
		width: 36,
		padding: 6,
		boxSizing: 'border-box',
		position: 'absolute',
		top: 0,
		left: 0,
		color: theme.palette.commonLite,
		[theme.breakpoints.down('md')]: {
			left: 'auto',
			right: 0
		},
	}
});

class LiveSearch extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {
		const { classes, theme } = this.props;

		return (
			<div className={ classes.box + (this.props.mobile ? ' mobile-ls' : '')}>
				<TextField
					fullWidth
					defaultValue={''}
					placeholder={'Поиск. Например: метанчик'}
					InputProps={{
						disableUnderline: true,
						classes: this.props.mobile ?
						{
							input: classes.textFieldInputAlt,
						} : {
							root: classes.textFieldRoot  + ' fullwidth',
							input: classes.textFieldInput,
						}
					}}
				/>
				<IconButton className={classes.icon} >
					<SearchIcon />
				</IconButton>
			</div>
		)
	}
}

export default withStyles(styles, { withTheme: true })(LiveSearch);