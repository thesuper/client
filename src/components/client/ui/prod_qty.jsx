import React from "react";
import autoBind from "react-autobind";
import {withStyles} from "material-ui/styles/index";
import TextField from 'material-ui/TextField';
import Input, { InputAdornment } from 'material-ui/Input';
import MoreIcon from 'material-ui-icons/ExpandMore';
import LessIcon from 'material-ui-icons/ExpandLess';
import IconButton from 'material-ui/IconButton';

const styles = theme => ({
	root: {
		width:110,
		height: '100%',
		position: 'relative',
		border: '1px solid rgba(0, 0, 0, 0.32)',
		boxSizing: 'border-box',
		borderRadius: 3,
		padding: '4px 8px',
	},
	textField: {
		width: '100%',
		marginRight: 8
	},
	less: {
		position: 'absolute',
		width: 28,
		height: 28,
		right: -36,
		top: -2
	},
	more: {
		position: 'absolute',
		width: 28,
		height: 28,
		right: -36,
		top: 29
	}
});

class QtyInput extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			value: this.props.value || 1
		};
		this.element = null;
		autoBind(this);
	}

	checkValue(value) {
		let val = parseInt(value);
		if (!isNaN(val)) {
			if (val < this.props.min) {
				val = this.props.min;
			} else if (val > this.props.max) {
				val = this.props.max;
			}
		} else {
			val = '';
		}
		return val;
	}

	handleChange(event) {
		this.setState({
			value: this.checkValue(event.target.value),
		});
	};

	changeValue(change) {
		this.setState({
			value: this.checkValue(this.state.value + change),
		});
	}

	render() {

		const { classes } = this.props;

		return (
			<div className={classes.root}>
				<TextField
					label={this.props.label || 'Количество'}
					className={classes.textField}
					value={this.state.value}
					onChange={this.handleChange}
					margin={this.props.margin}
					InputProps={{
						disableUnderline: true,
						endAdornment: <InputAdornment position="end">шт.</InputAdornment>
					}}
					InputLabelProps={{
						shrink: true
					}}
				/>
				<IconButton className={classes.less} onClick={ () => {this.changeValue(1)} }>
					<LessIcon />
				</IconButton>
				<IconButton className={classes.more} onClick={ () => {this.changeValue(-1)} }>
					<MoreIcon />
				</IconButton>
			</div>
		);
	}
}

export default withStyles(styles, { withTheme: true })(QtyInput);