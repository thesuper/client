import React from "react";
import autoBind from "react-autobind";
import {withStyles} from "material-ui/styles/index";
import TextField from 'material-ui/TextField';
import Input, { InputAdornment } from 'material-ui/Input';
import MoreIcon from 'material-ui-icons/ExpandMore';
import LessIcon from 'material-ui-icons/ExpandLess';
import IconButton from 'material-ui/IconButton';

const styles = theme => {

	const {btnLess, btnMore} = theme.palette;
	return {
		root: {
			width: 110,
			height: '100%',
			position: 'relative',
			boxSizing: 'border-box'
		},
		textField: {
			width: 70,
			marginRight: 8
		},
		less: {
			position: 'absolute',
			width: 28,
			height: 28,
			right: -8,
			top: 18,
			...btnLess
		},
		more: {
			position: 'absolute',
			width: 28,
			height: 28,
			right: -48,
			top: 18,
			...btnMore
		},
		adornment: {
			fontSize: 12
		},
		endAdornment: {
			maxHeight: 20
		},
		input: {
			paddingBottom: 0
		}
	}
};

class QtyInput extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			value: this.props.value || 1
		};
		autoBind(this);
	}

	checkValue(value) {
		let val = parseInt(value);
		if (!isNaN(val)) {
			if (val < this.props.min) {
				val = this.props.min;
			} else if (val > this.props.max) {
				val = this.props.max;
			}
		} else {
			val = '';
		}
		return val;
	}

	handleChange(event) {
		this.setState({
			value: this.checkValue(event.target.value),
		});
	};

	changeValue(change) {
		this.setState({
			value: this.checkValue(this.state.value + change),
		});
	}

	render() {

		const { classes } = this.props;

		return (
			<div className={classes.root}>
				<TextField
					label={this.props.label || 'Количество'}
					className={classes.textField}
					value={this.state.value}
					onChange={this.handleChange}
					margin={this.props.margin}
					InputProps={{
						disableUnderline: true,
						endAdornment: <InputAdornment position="end" className={classes.endAdornment}><span className={classes.adornment}>шт.</span></InputAdornment>,
						inputProps: {
							className: classes.input
						}
					}}
					InputLabelProps={{
						shrink: true
					}}
				/>
				<IconButton className={classes.less} onClick={ () => {this.changeValue(-1)} }>
					<MoreIcon />
				</IconButton>
				<IconButton className={classes.more} onClick={ () => {this.changeValue(1)} }>
					<LessIcon />
				</IconButton>
			</div>
		);
	}
}

export default withStyles(styles, { withTheme: true })(QtyInput);