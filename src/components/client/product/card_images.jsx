import React from 'react';
import { withStyles } from 'material-ui/styles';
import commonStyles from 'Theme/client/common_styles';
import { Carousel } from 'react-responsive-carousel';
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';

const cStyles = theme => {
	return {
		container: {
			width: theme.dimensions.productImageSize,
			height: theme.dimensions.productImageSize,
			float: 'right',
			[theme.breakpoints.down('md')]: {
				width: '60%',
				height: 'auto',
			},
		},
	}
};

class ProductImages extends React.Component {

	render() {

		const { classes } = this.props;

		return (
			<div className={classes.container}>
				<Carousel showThumbs={false}
				          infiniteLoop={true}
				          showStatus={false}
				>
					<div>
						<img src="/assets/img/prods/1.jpg" />
					</div>
					<div>
						<img src="/assets/img/prods/2.jpg" />
					</div>
					<div>
						<img src="/assets/img/prods/3.jpg" />
					</div>
				</Carousel>
			</div>
		)
	}
}

export default withStyles(cStyles, { withTheme: true })(ProductImages);