import React from 'react';
import autoBind from 'react-autobind';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';

const styles = theme => {
	return {
		title: {
			fontSize: 22,
			lineHeight: '28px',
			fontWeight: 'normal'
		},
		text: {

		}
	}
};

class ProductDescription extends React.Component {
	render() {

		const { classes } = this.props;

		return (
			<div className={this.props.className}>
				<Typography type="title" className={classes.title} gutterBottom>
					Побочные эффекты
				</Typography>
				<Typography className={classes.text} gutterBottom>
					Прежде чем принимать метандиенон и приступать к тренировкам необходимо пройти тщательное медицинское обследование и получить консультацию у врача. Своевременное знание о возможных побочных эффектах от приема данного стероида и методах их устранения без сомнения поможет избежать множества проблем в будущем.
				</Typography>
			</div>
		)
	}
}

export default withStyles(styles, { withTheme: true })(ProductDescription);