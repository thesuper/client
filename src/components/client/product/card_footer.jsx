import React from 'react';
import autoBind from 'react-autobind';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Hidden from 'material-ui/Hidden';
import Grid from 'material-ui/Grid';

const styles = theme => {

	const unit = theme.spacing.unit;
	const halfUnit = unit / 2;

	return {
		root: {
			flexGrow: 1,
		},
		button: {
			border: '1px solid rgba(0, 0, 0, 0.48)',
			boxSizing: 'border-box',
			borderRadius: 3,
			margin: `0 ${unit}px ${unit}px ${unit}px`,
			[theme.breakpoints.down('md')]: {
				margin: `0 ${halfUnit}px ${unit}px ${halfUnit}px`,
				padding:`${halfUnit}px ${unit}px`,
				fontSize: 12,
				minHeight: 32
			},
			'&:first-child': {
				marginLeft: 0
			},
			'&:last-child': {
				marginRight: 0
			},
		}
	}
};

class ProductFooter extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {

		const { classes } = this.props;

		return (
			<Grid container className={classes.root} justify="center" spacing={16}>

				<Hidden key="btn1" mdDown>
					<Button className={classes.button}>ЧИТАТЬ ОТЗЫВЫ</Button>
					<Button className={classes.button}>САЙТ ПРОИЗВОДИТЕЛЯ</Button>
					<Button className={classes.button}>ПРОВЕРИТЬ ПОДЛИННОСТЬ</Button>
				</Hidden>,
				<Hidden key="btn2" mdUp>
					<Button className={classes.button}>ОТЗЫВЫ</Button>
					<Button className={classes.button}>ПРОИЗВОДИТЕЛЬ</Button>
					<Button className={classes.button}>ПОДЛИННОСТЬ</Button>
				</Hidden>

			</Grid>
		)
	}
}

export default withStyles(styles, { withTheme: true })(ProductFooter);