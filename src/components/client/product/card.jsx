import React from 'react';
import autoBind from 'react-autobind';
import { withStyles } from 'material-ui/styles';
import ProductName from 'Comps/client/product/card_name';
import ProductImages from 'Comps/client/product/card_images';
import ProductProps from 'Comps/client/product/card_props';
import ProductAvail from 'Comps/client/product/card_avail';
import ProductQty from 'Comps/client/product/card_qty';
import ProductFooter from 'Comps/client/product/card_footer';
import ProductDescription from 'Comps/client/product/card_descr';
import ProductOffersVertical from 'Comps/client/product/card_offers_vert';
import Hidden from 'material-ui/Hidden';

const styles = theme => ({
	cardRoot: {
		backgroundColor: theme.palette.common.fullWhite,
		width: '100%',
		padding: theme.spacing.unit * 3,
		border: '1px solid rgba(0, 0, 0, 0.16)',
		boxSizing: 'border-box',
		boxShadow: '0px 16px 48px rgba(31, 31, 56, 0.08)',
		borderRadius: theme.other.raisedPrimaryRadius,
		overflow: 'hidden',
		marginBottom: theme.spacing.unit * 4,
		[theme.breakpoints.down('md')]: {
			padding: theme.spacing.unit * 3
		},
	},
	footer: {
		width: '100%',
		marginBottom: theme.spacing.unit * 4,
		[theme.breakpoints.down('md')]: {
			marginBottom: theme.spacing.unit * 3,
		}
	},
	description: {
		marginBottom: theme.spacing.unit * 4,
		[theme.breakpoints.down('md')]: {
			marginBottom: theme.spacing.unit * 12,
		}
	}
});

class ProductCard extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {

		const { classes } = this.props;

		return ([
			<Hidden mdDown>
				<ProductOffersVertical />
			</Hidden>,
			<section key="card1" className={classes.cardRoot}>
				<ProductName />
				<ProductProps />
				<ProductImages />
				<ProductAvail />
				<ProductQty />
			</section>,
			<section key="card2" className={classes.footer}>
				<ProductFooter />
			</section>,
			<ProductDescription key="card3" className={classes.description} />,
		])
	}
}

export default withStyles(styles, { withTheme: true })(ProductCard);