import React from 'react';
import autoBind from 'react-autobind';
import { withStyles } from 'material-ui/styles';
import commonStyles from 'Theme/client/common_styles';

const styles = theme => {

	const { group } = commonStyles;

	return {
		root: {
			width: `calc(100% - ${theme.dimensions.productImageSize}px)`,
			boxSizing: 'border-box',
			paddingRight: theme.spacing.unit*2,
			paddingBottom: theme.spacing.unit*2,
			float: 'left',
			marginTop: -4,
			[theme.breakpoints.down('md')]: {
				width: '40%'
			}
		},
		caption: {
			display: 'block',
			padding: '2px 0',
			lineHeight: '18px',
			minHeight: 18,
			fontSize: 14,
			color: theme.palette.commonLite,
			marginTop: theme.spacing.unit*3,
			'&:first-child': {
				marginTop: 0,
				paddingTop: 0
			},
			[theme.breakpoints.down('md')]: {
				fontSize: 12,
				minHeight: 16,
				lineHeight: '16px',
				padding: '1px 0',
				marginTop: theme.spacing.unit,
			},
		},
		prop: {
			fontSize: 20,
			lineHeight: '24px',
			fontWeight: 500,
			display: 'block',
			[theme.breakpoints.down('md')]: {
				fontSize: 16,
				lineHeight: '20px',
				fontWeight: 'normal'
			},
		}
	}
};

class ProductProps extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {

		const { classes } = this.props;

		return (
			<div className={classes.root}>
				<div className={classes.caption}>Категория</div>
				<div className={classes.prop}>Станозол</div>

				<div className={classes.caption}>Производитель</div>
				<div className={classes.prop}>SP Laboratories</div>

				<div className={classes.caption}>Упаковка</div>
				<div className={classes.prop}>10 мл.</div>

				<div className={classes.caption}>Концентрация
					действующего вещества</div>
				<div className={classes.prop}>50 мг / мл.</div>

			</div>
		)
	}
}

export default withStyles(styles, { withTheme: true })(ProductProps);