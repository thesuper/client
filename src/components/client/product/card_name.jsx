import React from 'react';
import autoBind from 'react-autobind';
import { withStyles } from 'material-ui/styles';
import commonStyles from 'Theme/client/common_styles';
import Typography from 'material-ui/Typography';
import RoubleIcon from 'Comps/client/cart/rur';
import RurThinIcon from 'mdi-material-ui/CurrencyRub';
import Hidden from 'material-ui/Hidden';

const textColor = 'rgba(0, 0, 0, 0.87)';

const styles = theme => {

	const { group } = commonStyles;

	return {
		productName: {
			minHeight: 52,
			lineHeight: '52px',
			width: 'calc(100% - 200px)',
			float: 'left',
			fontSize: 45,
			color: textColor,
			marginBottom: theme.spacing.unit,
			[theme.breakpoints.down('md')]: {
				minHeight: 20,
				lineHeight: '18px',
				width: 'calc(100% - 120px)',
				fontSize: 18,
				marginBottom: theme.spacing.unit / 2,
			},
		},
		productPrice: {
			height: 52,
			lineHeight: '52px',
			width: '180px',
			float: 'right',
			fontSize: 45,
			fontWeight: 'bold',
			color: textColor,
			paddingRight: 44,
			boxSizing: 'border-box',
			textAlign: 'right',
			position: 'relative',
			marginBottom: 8,
			[theme.breakpoints.down('md')]: {
				height: 32,
				lineHeight: '32px',
				fontSize: 32,
				width: 120,
				paddingRight: 19,
				fontWeight: 'normal'
			},
		},
		productCurrency: {
			position: 'absolute',
			right: 0,
			top: 8,
			width: 36,
			height: 36,
			[theme.breakpoints.down('md')]: {
				width: 32,
				height: 32,
				right: -5,
				top: 1,
			},
		},
		productDescription: {
			fontSize: 24,
			lineHeight: '32px',
			minHeight: 32,
			float: 'left',
			color: theme.palette.commonLite,
			width: 'calc(100% - 150px)',
			[theme.breakpoints.down('md')]: {
				width: 'calc(100% - 120px)',
				fontSize: 12,
				lineHeight: '16px',
				minHeight: 16,
			},
		},
		productOldPrice: {
			width: 130,
			float: 'right',
			fontSize: 24,
			lineHeight: '32px',
			minHeight: 32,
			color: '#FF1744',
			textDecoration: 'line-through',
			textAlign: 'right',
			[theme.breakpoints.down('md')]: {
				width: 120,
				fontSize: 16,
				lineHeight: '14px',
				minHeight: 16,
			},
		},
		productOldCurrency: {
			height: 23,
			width: 24,
			position: 'relative',
			top: 4,
			[theme.breakpoints.down('md')]: {
				height: 16,
				lineHeight: '16px',
				width: 16,
				top: 2,
			},
		},
		group: {
			marginBottom: theme.spacing.unit * 3,
			[theme.breakpoints.down('md')]: {
				marginBottom: theme.spacing.unit * 2
			},
			...group
		}
	}
};

class ProductName extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {

		const { classes } = this.props;

		return (
			<div className={classes.group}>
				<Typography type="display1" className={classes.productName}>
					SP Stanoject
				</Typography>
				<Typography type="title" noWrap className={classes.productPrice}>
					9999
					<Hidden mdDown>
						<RoubleIcon className={classes.productCurrency} />
					</Hidden>
					<Hidden mdUp>
						<RurThinIcon className={classes.productCurrency} />
					</Hidden>
				</Typography>
				<Typography className={classes.productDescription}>
					10 ml × 50 mg/ml от SP Laboratories
				</Typography>
				<Typography className={classes.productOldPrice}>
					10000<RurThinIcon className={classes.productOldCurrency} />
				</Typography>

			</div>
		)
	}
}

export default withStyles(styles, { withTheme: true })(ProductName);