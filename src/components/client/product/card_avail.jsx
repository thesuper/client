import React from 'react';
import autoBind from 'react-autobind';
import { withStyles } from 'material-ui/styles';
import { LinearProgress } from 'material-ui/Progress';
import green from "material-ui/colors/green";
import red from "material-ui/colors/red";
import yellow from "material-ui/colors/yellow";

const styles = theme => {

	return {
		root: {
			width: theme.dimensions.productImageSize,
			marginTop: theme.spacing.unit,
			[theme.breakpoints.down('md')]: {
				width: '60%'
			},
			float: 'right'
		},
		caption: {
			display: 'block',
			padding: '2px 0',
			lineHeight: '18px',
			minHeight: 18,
			fontSize: 14,
			textAlign: 'right',
			color: theme.palette.commonLite,
			marginTop: theme.spacing.unit*3,
			'&:first-child': {
				marginTop: 0,
				paddingTop: 0
			},
			[theme.breakpoints.down('md')]: {
				fontSize: 12,
				minHeight: 16,
				lineHeight: '16px',
				padding: '1px 0',
				marginTop: theme.spacing.unit,
			},
		},
		avalable: {
			fontSize: 20,
			lineHeight: '24px',
			fontWeight: 500,
			display: 'block',
			textAlign: 'right',
			[theme.breakpoints.down('md')]: {
				fontSize: 16,
				lineHeight: '20px',
				fontWeight: 'normal'
			},
		},
		progress: {
			marginTop: theme.spacing.unit * 2,
			width: '100%'
		},
		many: {
			backgroundColor: green[50]
		},
		manyBg: {
			backgroundColor: green[500]
		},
		avarage: {
			backgroundColor: yellow[50]
		},
		avarageBg: {
			backgroundColor: yellow[500]
		},
		few: {
			backgroundColor: red[50]
		},
		fewBg: {
			backgroundColor: red[500]
		}
	}
};

class ProductAvailable extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {

		const { classes } = this.props;

		return (
			<div className={classes.root}>
				<div className={classes.caption}>На складе</div>
				<div className={classes.avalable} style={{color:green[500]}}>Много</div>
				<div className={classes.progress}>
					<LinearProgress classes={{primaryColor: classes.many, primaryColorBar: classes.manyBg }} mode="determinate" value={85} />
				</div>
			</div>
		)
	}
}

export default withStyles(styles, { withTheme: true })(ProductAvailable);