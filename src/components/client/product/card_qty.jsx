import React from 'react';
import autoBind from 'react-autobind';
import { withStyles } from 'material-ui/styles';
import commonStyles from 'Theme/client/common_styles';
import QtyInput from 'Comps/client/ui/prod_qty';
import QtyInputHorizontal from 'Comps/client/ui/qty_horiz';
import Button from 'material-ui/Button';
import ShoppingCart from 'material-ui-icons/ShoppingCart';
import AppBar from 'material-ui/AppBar';
import Hidden from 'material-ui/Hidden';

const styles = theme => {

	const { group, boxHalf, pullLeft } = commonStyles;

	return {
		group: {
			paddingTop: theme.spacing.unit*3,
			clear: 'both',
			overflow: 'visible',
			[theme.breakpoints.down('md')]: {
				display: 'none'
			},
		},
		button: {
			float: 'right',
			width: theme.dimensions.productImageSize,
			[theme.breakpoints.down('md')]: {
				width: '100%',
				height: 56,
				boxSizing: 'border-box',
				fontSize: 18,
			},
			fontSize: 20,
			textTransform: 'none',
			paddingTop: 15,
			paddingBottom: 15,
			paddingLeft: 48,
			fontWeight: 'normal',
			backgroundColor: '#212121',
			color: '#fff',
			'&:hover': {
				backgroundColor: '#555',
			},
		},
		input: {
			float: 'left',
			width: `calc(99% - ${theme.dimensions.productImageSize}px)`,
			[theme.breakpoints.down('md')]: {
				width: '39%'
			}
		},
		buttonIcon: {
			position: 'absolute',
			left: theme.spacing.unit *2
		},
		raisedPrimary: {
			boxShadow: '0px 4px 8px rgba(23, 29, 33, 0.16)',
			borderRadius: 3,
			'&:active': {
				boxShadow: '0px 1px 3px rgba(23, 29, 33, 0.16)',
			}
		},
		boxLeft: {
			...pullLeft,
			width: 176,
			height: '100%'
		},
		boxRight: {
			...pullLeft,
			width: 'calc(100% - 176px)',
		},
		cartbar: {
			top: 'auto',
			bottom: 0,
			zIndex: theme.zIndex.appBar-30
		},
		row: {
			width: '100%',
			overflow: 'hidden',
			height: 72,
			boxSizing: 'border-box',
			padding: theme.spacing.unit
		}
	}
};

class ProductQty extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {

		const { classes } = this.props;

		return ([
			<div key="qty1" className={classes.group}>
				<div className={classes.input}>
					<QtyInput />
				</div>
				<Button raised
					classes={{raisedPrimary: classes.raisedPrimary}}
					className={classes.button}
					color="primary"
				>
					<ShoppingCart className={classes.buttonIcon} />
					Купить
				</Button>
			</div>,
			<Hidden key="qty2" mdUp>
				<AppBar position="fixed" className={classes.cartbar} color="inherit">
					<div className={classes.row}>
						<div className={classes.boxLeft}>
							<QtyInputHorizontal />
						</div>
						<div className={classes.boxRight}>
							<Button raised
							        classes={{raisedPrimary: classes.raisedPrimary}}
							        className={classes.button}
							        color="primary"
							>
								<ShoppingCart className={classes.buttonIcon} />
								Купить
							</Button>
						</div>
					</div>
				</AppBar>
			</Hidden>
		]);
	}
}

export default withStyles(styles, { withTheme: true })(ProductQty);