//common jss styles
const commonStyles = {
	group: {
		overflow: 'hidden',
		'&:after': {
			content: "",
			display: 'table',
			clear: 'both'
		}
	},
	pullLeft: {
		float: 'left'
	},
	pullRight: {
		float: 'right'
	},
	boxHalf: {
		boxSizing: 'border-box',
		width: '50%',
		padding: 8,
		float: 'left'
	}
};

export default commonStyles;

