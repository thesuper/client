import {createMuiTheme} from "material-ui/styles/index";
import blue from "material-ui/colors/blue";

export default createMuiTheme({
	palette: {
		primary: blue,
		iconsPrimary: '#FF5252',
		commonLite: 'rgba(0, 0, 0, 0.54)',
		buttonsAltTextColor: '#fff',
		selectedBackgroundColor: 'linear-gradient(270deg, #2196F3 0%, #9857F2 100%)',
		background: {
			default: '#F2F2F2'
		},
		raisedPrimaryShadow: '0px 4px 8px rgba(33, 150, 243, 0.24)',
		raisedPrimaryShadowActive: '0px 1px 2px rgba(33, 150, 243, 0.24)',
		btnLess: {
			background: '#FFCDD2',
			color: '#EF5350'
		},
		btnMore: {
			background: '#A5D6A7',
			color: '#2E7D32'
		}
	},
	other: {
		raisedPrimaryRadius: 3
	},
	dimensions: {
		appBarHeight: 65,
		productImageSize: 325
	},
	overrides: {
		MuiAppBar: {
			root: {
				background: '#fff',
				color: '#333'
			}
		},
		MuiBackdrop: {
			root: {
				backgroundColor: 'rgba(248,248,248,0.5)'
			}
		}
	}
});