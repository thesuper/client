import React from 'react';
import autoBind from 'react-autobind';
import { withStyles } from 'material-ui/styles';
import {Helmet} from "react-helmet";
import ResponsiveNav from 'Comps/client/chrome/navigation';
import ProductCard from "Comps/client/product/card";

const styles = theme => ({
	pageRoot: {
		backgroundColor: theme.palette.background.default,
		width: 640,
		overflow: 'visible',
		padding: 0,
		margin: `${theme.dimensions.appBarHeight}px auto 37px auto`,
		boxSizing: 'border-box',
		minHeight: `calc(100% - ${theme.dimensions.appBarHeight}px)`,
		paddingTop: 37,
		[theme.breakpoints.down('md')]: {
			padding: 8
		},
		position: 'relative'
	}
});

class IndexPage extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	componentDidMount() {

	}

	render() {

		const { classes } = this.props;

		return (
			<div className={'page-client page-client-index'}>
				<Helmet>
					<title>React Shop - Index page</title>
				</Helmet>
				<ResponsiveNav />
				<main className={classes.pageRoot}>
					<ProductCard />
				</main>
			</div>
		)
	}
}

export default withStyles(styles, { withTheme: true })(IndexPage);