<?php

class App {

	protected $dir;
	protected $js;
	protected $css;
	protected $view;

	public function __construct() {

		$this->dir = __DIR__.'/';
		$this->view = $this->dir.'view/';
		$this->js = realpath($this->dir.'../assets/js').'/';
		$this->css = realpath($this->dir.'../assets/css').'/';

	}

	public function echo_r($data, $dump = FALSE, $tofile = FALSE, $fname = 'output.txt') {
		if ($tofile) ob_start();
		else echo '<pre>';

		if ($dump) {
			var_dump($data);
		} else {
			print_r($data);
		}

		if ($tofile) {
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/$fname", ob_get_contents());
			ob_end_clean();
		}
		else echo '</pre>';
	}

	public function view($file, $vars = array()) {

		if (strtolower(substr($file, -4)) != ".php") {
			$file .= '.php';
		}

		$view_path = $this->view.$file;
		if (!is_file($view_path)) {
			throw new Exception('View not found: '.$file);
		}

		if (!is_array($vars)) $vars = (array)$vars;
		ob_start();
		extract($vars);
		require($view_path);

		return ob_get_clean();
	}

	protected function mtimes($files = array()) {
		$result = array();
		$from = array('#JS#', '#CSS#');
		$to = array($this->js, $this->css);
		foreach($files as $file) {
			$path = str_ireplace($from, $to, $file);
			$key = str_ireplace($from, '', $file);
			if (file_exists($path)) {
				$result[$key] = filemtime($path);
			}
		}
		return $result;
	}

	public function minify_on() {
		ob_start(function ($buffer) {
			$search = array(
				'/\>[^\S ]+/s',     // strip whitespaces after tags, except space
				'/[^\S ]+\</s',     // strip whitespaces before tags, except space
				'/(\s)+/s',         // shorten multiple whitespace sequences
				'/<!--(.|\s)*?-->/' // Remove HTML comments
			);
			$replace = array(
				'>',
				'<',
				'\\1',
				''
			);
			return str_replace(array("\n", "\r", "\t"), '', preg_replace($search, $replace, $buffer));
		});
	}

}