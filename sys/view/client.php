<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ParkerLabs</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?=$preloader_css?>
</head>
<body>
<div id="clientapp-root" class="client-root">
<style type="text/css">
.client-root > div {
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background-color: #f0f0f0;
	background-image: repeating-linear-gradient(45deg, transparent, transparent 70px, rgba(255,255,255,.3) 70px, rgba(255,255,255,.3) 140px);
	color: #333;
}
.client-root > div > div {
	position: absolute;
	top: 48%;
	left: 0;
	width: 100%;
	text-align: center;
}
.client-root > div > div > div {
	width: 140px;
	display: inline-block;
	height: 48px;
	position: relative;
}
.client-root .la-dark {
	color: #999;}
.client-root > div > div > div > span {
	font-family: Roboto, Arial;
	position: absolute;
	top: 7px;
	right: 0;
	letter-spacing: 1px;
	font-size: 14px;
}
</style>
	<div>
		<div>
			<div>
				<div class="la-timer la-dark"><div></div></div><span> Загружаем...</span>
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="/assets/css/client.css?<?=$modified['client.css']?>">
<script src="/assets/js/vendor.js?<?=$modified['vendor.js']?>"></script>
<script src="/assets/js/index.js?<?=$modified['index.js']?>"></script>
</body>
</html>