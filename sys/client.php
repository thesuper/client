<?php
require(__DIR__.'/app.php');

class ClientApp extends App{

	public function render() {

		$files = array(
			'#JS#index.js',
			'#JS#vendor.js',
			'#CSS#client.css',
		);

		try {
			echo $this->view('client', array(
				'modified' => $this->mtimes($files),
				'preloader_css' => $this->view('preloader_css')
			));
		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}
}

